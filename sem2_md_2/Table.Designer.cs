﻿namespace sem2_md_2
{
    partial class Table
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Table));
            this.button1 = new System.Windows.Forms.Button();
            this.diceBox5 = new System.Windows.Forms.PictureBox();
            this.cpuBox3 = new System.Windows.Forms.PictureBox();
            this.cpuBox5 = new System.Windows.Forms.PictureBox();
            this.cpuBox1 = new System.Windows.Forms.PictureBox();
            this.playerBox = new System.Windows.Forms.PictureBox();
            this.cpuBox2 = new System.Windows.Forms.PictureBox();
            this.cpuBox4 = new System.Windows.Forms.PictureBox();
            this.cpuBox6 = new System.Windows.Forms.PictureBox();
            this.diceBox3 = new System.Windows.Forms.PictureBox();
            this.diceBox1 = new System.Windows.Forms.PictureBox();
            this.dicePlayer = new System.Windows.Forms.PictureBox();
            this.diceBox2 = new System.Windows.Forms.PictureBox();
            this.diceBox4 = new System.Windows.Forms.PictureBox();
            this.diceBox6 = new System.Windows.Forms.PictureBox();
            this.cpuLabel5 = new System.Windows.Forms.Label();
            this.cpuLabel3 = new System.Windows.Forms.Label();
            this.cpuLabel1 = new System.Windows.Forms.Label();
            this.playerLabel = new System.Windows.Forms.Label();
            this.cpuLabel2 = new System.Windows.Forms.Label();
            this.cpuLabel4 = new System.Windows.Forms.Label();
            this.cpuLabel6 = new System.Windows.Forms.Label();
            this.cpuTot5 = new System.Windows.Forms.Label();
            this.cpuTot3 = new System.Windows.Forms.Label();
            this.cpuTot1 = new System.Windows.Forms.Label();
            this.playerTot = new System.Windows.Forms.Label();
            this.cpuTot2 = new System.Windows.Forms.Label();
            this.cpuTot4 = new System.Windows.Forms.Label();
            this.cpuTot6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.stopButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dicePlayer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox6)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(222, 271);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Enter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // diceBox5
            // 
            this.diceBox5.BackColor = System.Drawing.Color.Transparent;
            this.diceBox5.Location = new System.Drawing.Point(28, 145);
            this.diceBox5.Name = "diceBox5";
            this.diceBox5.Size = new System.Drawing.Size(32, 32);
            this.diceBox5.TabIndex = 8;
            this.diceBox5.TabStop = false;
            // 
            // cpuBox3
            // 
            this.cpuBox3.BackColor = System.Drawing.Color.Transparent;
            this.cpuBox3.Image = ((System.Drawing.Image)(resources.GetObject("cpuBox3.Image")));
            this.cpuBox3.Location = new System.Drawing.Point(82, 53);
            this.cpuBox3.Name = "cpuBox3";
            this.cpuBox3.Size = new System.Drawing.Size(64, 64);
            this.cpuBox3.TabIndex = 2;
            this.cpuBox3.TabStop = false;
            // 
            // cpuBox5
            // 
            this.cpuBox5.BackColor = System.Drawing.Color.Transparent;
            this.cpuBox5.Image = ((System.Drawing.Image)(resources.GetObject("cpuBox5.Image")));
            this.cpuBox5.Location = new System.Drawing.Point(12, 53);
            this.cpuBox5.Name = "cpuBox5";
            this.cpuBox5.Size = new System.Drawing.Size(64, 64);
            this.cpuBox5.TabIndex = 1;
            this.cpuBox5.TabStop = false;
            // 
            // cpuBox1
            // 
            this.cpuBox1.BackColor = System.Drawing.Color.Transparent;
            this.cpuBox1.Image = ((System.Drawing.Image)(resources.GetObject("cpuBox1.Image")));
            this.cpuBox1.Location = new System.Drawing.Point(152, 53);
            this.cpuBox1.Name = "cpuBox1";
            this.cpuBox1.Size = new System.Drawing.Size(64, 64);
            this.cpuBox1.TabIndex = 15;
            this.cpuBox1.TabStop = false;
            // 
            // playerBox
            // 
            this.playerBox.BackColor = System.Drawing.Color.Transparent;
            this.playerBox.Image = ((System.Drawing.Image)(resources.GetObject("playerBox.Image")));
            this.playerBox.Location = new System.Drawing.Point(222, 53);
            this.playerBox.Name = "playerBox";
            this.playerBox.Size = new System.Drawing.Size(64, 64);
            this.playerBox.TabIndex = 16;
            this.playerBox.TabStop = false;
            // 
            // cpuBox2
            // 
            this.cpuBox2.BackColor = System.Drawing.Color.Transparent;
            this.cpuBox2.Image = ((System.Drawing.Image)(resources.GetObject("cpuBox2.Image")));
            this.cpuBox2.Location = new System.Drawing.Point(292, 53);
            this.cpuBox2.Name = "cpuBox2";
            this.cpuBox2.Size = new System.Drawing.Size(64, 64);
            this.cpuBox2.TabIndex = 17;
            this.cpuBox2.TabStop = false;
            // 
            // cpuBox4
            // 
            this.cpuBox4.BackColor = System.Drawing.Color.Transparent;
            this.cpuBox4.Image = ((System.Drawing.Image)(resources.GetObject("cpuBox4.Image")));
            this.cpuBox4.Location = new System.Drawing.Point(362, 53);
            this.cpuBox4.Name = "cpuBox4";
            this.cpuBox4.Size = new System.Drawing.Size(64, 64);
            this.cpuBox4.TabIndex = 18;
            this.cpuBox4.TabStop = false;
            // 
            // cpuBox6
            // 
            this.cpuBox6.BackColor = System.Drawing.Color.Transparent;
            this.cpuBox6.Image = ((System.Drawing.Image)(resources.GetObject("cpuBox6.Image")));
            this.cpuBox6.Location = new System.Drawing.Point(432, 53);
            this.cpuBox6.Name = "cpuBox6";
            this.cpuBox6.Size = new System.Drawing.Size(64, 64);
            this.cpuBox6.TabIndex = 19;
            this.cpuBox6.TabStop = false;
            // 
            // diceBox3
            // 
            this.diceBox3.BackColor = System.Drawing.Color.Transparent;
            this.diceBox3.Location = new System.Drawing.Point(98, 145);
            this.diceBox3.Name = "diceBox3";
            this.diceBox3.Size = new System.Drawing.Size(32, 32);
            this.diceBox3.TabIndex = 20;
            this.diceBox3.TabStop = false;
            // 
            // diceBox1
            // 
            this.diceBox1.BackColor = System.Drawing.Color.Transparent;
            this.diceBox1.Location = new System.Drawing.Point(168, 145);
            this.diceBox1.Name = "diceBox1";
            this.diceBox1.Size = new System.Drawing.Size(32, 32);
            this.diceBox1.TabIndex = 21;
            this.diceBox1.TabStop = false;
            // 
            // dicePlayer
            // 
            this.dicePlayer.BackColor = System.Drawing.Color.Transparent;
            this.dicePlayer.Location = new System.Drawing.Point(238, 145);
            this.dicePlayer.Name = "dicePlayer";
            this.dicePlayer.Size = new System.Drawing.Size(32, 32);
            this.dicePlayer.TabIndex = 22;
            this.dicePlayer.TabStop = false;
            // 
            // diceBox2
            // 
            this.diceBox2.BackColor = System.Drawing.Color.Transparent;
            this.diceBox2.Location = new System.Drawing.Point(308, 145);
            this.diceBox2.Name = "diceBox2";
            this.diceBox2.Size = new System.Drawing.Size(32, 32);
            this.diceBox2.TabIndex = 23;
            this.diceBox2.TabStop = false;
            // 
            // diceBox4
            // 
            this.diceBox4.BackColor = System.Drawing.Color.Transparent;
            this.diceBox4.Location = new System.Drawing.Point(378, 145);
            this.diceBox4.Name = "diceBox4";
            this.diceBox4.Size = new System.Drawing.Size(32, 32);
            this.diceBox4.TabIndex = 24;
            this.diceBox4.TabStop = false;
            // 
            // diceBox6
            // 
            this.diceBox6.BackColor = System.Drawing.Color.Transparent;
            this.diceBox6.Location = new System.Drawing.Point(448, 145);
            this.diceBox6.Name = "diceBox6";
            this.diceBox6.Size = new System.Drawing.Size(32, 32);
            this.diceBox6.TabIndex = 25;
            this.diceBox6.TabStop = false;
            // 
            // cpuLabel5
            // 
            this.cpuLabel5.AutoSize = true;
            this.cpuLabel5.BackColor = System.Drawing.Color.Transparent;
            this.cpuLabel5.ForeColor = System.Drawing.SystemColors.Control;
            this.cpuLabel5.Location = new System.Drawing.Point(28, 34);
            this.cpuLabel5.Name = "cpuLabel5";
            this.cpuLabel5.Size = new System.Drawing.Size(38, 13);
            this.cpuLabel5.TabIndex = 26;
            this.cpuLabel5.Text = "CPU 5";
            // 
            // cpuLabel3
            // 
            this.cpuLabel3.AutoSize = true;
            this.cpuLabel3.BackColor = System.Drawing.Color.Transparent;
            this.cpuLabel3.ForeColor = System.Drawing.SystemColors.Control;
            this.cpuLabel3.Location = new System.Drawing.Point(95, 34);
            this.cpuLabel3.Name = "cpuLabel3";
            this.cpuLabel3.Size = new System.Drawing.Size(38, 13);
            this.cpuLabel3.TabIndex = 27;
            this.cpuLabel3.Text = "CPU 3";
            // 
            // cpuLabel1
            // 
            this.cpuLabel1.AutoSize = true;
            this.cpuLabel1.BackColor = System.Drawing.Color.Transparent;
            this.cpuLabel1.ForeColor = System.Drawing.SystemColors.Control;
            this.cpuLabel1.Location = new System.Drawing.Point(165, 34);
            this.cpuLabel1.Name = "cpuLabel1";
            this.cpuLabel1.Size = new System.Drawing.Size(38, 13);
            this.cpuLabel1.TabIndex = 28;
            this.cpuLabel1.Text = "CPU 1";
            // 
            // playerLabel
            // 
            this.playerLabel.AutoSize = true;
            this.playerLabel.BackColor = System.Drawing.Color.Transparent;
            this.playerLabel.ForeColor = System.Drawing.SystemColors.Control;
            this.playerLabel.Location = new System.Drawing.Point(235, 34);
            this.playerLabel.Name = "playerLabel";
            this.playerLabel.Size = new System.Drawing.Size(36, 13);
            this.playerLabel.TabIndex = 29;
            this.playerLabel.Text = "Player";
            // 
            // cpuLabel2
            // 
            this.cpuLabel2.AutoSize = true;
            this.cpuLabel2.BackColor = System.Drawing.Color.Transparent;
            this.cpuLabel2.ForeColor = System.Drawing.SystemColors.Control;
            this.cpuLabel2.Location = new System.Drawing.Point(305, 34);
            this.cpuLabel2.Name = "cpuLabel2";
            this.cpuLabel2.Size = new System.Drawing.Size(38, 13);
            this.cpuLabel2.TabIndex = 30;
            this.cpuLabel2.Text = "CPU 2";
            // 
            // cpuLabel4
            // 
            this.cpuLabel4.AutoSize = true;
            this.cpuLabel4.BackColor = System.Drawing.Color.Transparent;
            this.cpuLabel4.ForeColor = System.Drawing.SystemColors.Control;
            this.cpuLabel4.Location = new System.Drawing.Point(375, 34);
            this.cpuLabel4.Name = "cpuLabel4";
            this.cpuLabel4.Size = new System.Drawing.Size(38, 13);
            this.cpuLabel4.TabIndex = 31;
            this.cpuLabel4.Text = "CPU 4";
            // 
            // cpuLabel6
            // 
            this.cpuLabel6.AutoSize = true;
            this.cpuLabel6.BackColor = System.Drawing.Color.Transparent;
            this.cpuLabel6.ForeColor = System.Drawing.SystemColors.Control;
            this.cpuLabel6.Location = new System.Drawing.Point(445, 34);
            this.cpuLabel6.Name = "cpuLabel6";
            this.cpuLabel6.Size = new System.Drawing.Size(38, 13);
            this.cpuLabel6.TabIndex = 32;
            this.cpuLabel6.Text = "CPU 6";
            // 
            // cpuTot5
            // 
            this.cpuTot5.AutoSize = true;
            this.cpuTot5.BackColor = System.Drawing.Color.Transparent;
            this.cpuTot5.Location = new System.Drawing.Point(28, 120);
            this.cpuTot5.Name = "cpuTot5";
            this.cpuTot5.Size = new System.Drawing.Size(35, 13);
            this.cpuTot5.TabIndex = 33;
            this.cpuTot5.Text = "label1";
            // 
            // cpuTot3
            // 
            this.cpuTot3.AutoSize = true;
            this.cpuTot3.BackColor = System.Drawing.Color.Transparent;
            this.cpuTot3.Location = new System.Drawing.Point(95, 120);
            this.cpuTot3.Name = "cpuTot3";
            this.cpuTot3.Size = new System.Drawing.Size(35, 13);
            this.cpuTot3.TabIndex = 33;
            this.cpuTot3.Text = "label1";
            // 
            // cpuTot1
            // 
            this.cpuTot1.AutoSize = true;
            this.cpuTot1.BackColor = System.Drawing.Color.Transparent;
            this.cpuTot1.Location = new System.Drawing.Point(165, 120);
            this.cpuTot1.Name = "cpuTot1";
            this.cpuTot1.Size = new System.Drawing.Size(35, 13);
            this.cpuTot1.TabIndex = 33;
            this.cpuTot1.Text = "label1";
            // 
            // playerTot
            // 
            this.playerTot.AutoSize = true;
            this.playerTot.BackColor = System.Drawing.Color.Transparent;
            this.playerTot.Location = new System.Drawing.Point(235, 120);
            this.playerTot.Name = "playerTot";
            this.playerTot.Size = new System.Drawing.Size(35, 13);
            this.playerTot.TabIndex = 33;
            this.playerTot.Text = "label1";
            // 
            // cpuTot2
            // 
            this.cpuTot2.AutoSize = true;
            this.cpuTot2.BackColor = System.Drawing.Color.Transparent;
            this.cpuTot2.Location = new System.Drawing.Point(305, 120);
            this.cpuTot2.Name = "cpuTot2";
            this.cpuTot2.Size = new System.Drawing.Size(35, 13);
            this.cpuTot2.TabIndex = 33;
            this.cpuTot2.Text = "label1";
            // 
            // cpuTot4
            // 
            this.cpuTot4.AutoSize = true;
            this.cpuTot4.BackColor = System.Drawing.Color.Transparent;
            this.cpuTot4.Location = new System.Drawing.Point(375, 120);
            this.cpuTot4.Name = "cpuTot4";
            this.cpuTot4.Size = new System.Drawing.Size(35, 13);
            this.cpuTot4.TabIndex = 33;
            this.cpuTot4.Text = "label1";
            // 
            // cpuTot6
            // 
            this.cpuTot6.AutoSize = true;
            this.cpuTot6.BackColor = System.Drawing.Color.Transparent;
            this.cpuTot6.Location = new System.Drawing.Point(445, 120);
            this.cpuTot6.Name = "cpuTot6";
            this.cpuTot6.Size = new System.Drawing.Size(35, 13);
            this.cpuTot6.TabIndex = 33;
            this.cpuTot6.Text = "label1";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(222, 245);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(75, 20);
            this.textBox1.TabIndex = 34;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(176, 201);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 13);
            this.label1.TabIndex = 35;
            this.label1.Text = "Enter number of CPU players(1-6)";
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(308, 271);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(75, 23);
            this.stopButton.TabIndex = 36;
            this.stopButton.Text = "Stop";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Visible = false;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // Table
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::sem2_md_2.Properties.Resources.bg;
            this.ClientSize = new System.Drawing.Size(513, 328);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.cpuTot6);
            this.Controls.Add(this.cpuTot4);
            this.Controls.Add(this.cpuTot2);
            this.Controls.Add(this.playerTot);
            this.Controls.Add(this.cpuTot1);
            this.Controls.Add(this.cpuTot3);
            this.Controls.Add(this.cpuTot5);
            this.Controls.Add(this.cpuLabel6);
            this.Controls.Add(this.cpuLabel4);
            this.Controls.Add(this.cpuLabel2);
            this.Controls.Add(this.playerLabel);
            this.Controls.Add(this.cpuLabel1);
            this.Controls.Add(this.cpuLabel3);
            this.Controls.Add(this.cpuLabel5);
            this.Controls.Add(this.diceBox6);
            this.Controls.Add(this.diceBox4);
            this.Controls.Add(this.diceBox2);
            this.Controls.Add(this.dicePlayer);
            this.Controls.Add(this.diceBox1);
            this.Controls.Add(this.diceBox3);
            this.Controls.Add(this.cpuBox6);
            this.Controls.Add(this.cpuBox4);
            this.Controls.Add(this.cpuBox2);
            this.Controls.Add(this.playerBox);
            this.Controls.Add(this.cpuBox1);
            this.Controls.Add(this.diceBox5);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cpuBox3);
            this.Controls.Add(this.cpuBox5);
            this.Name = "Table";
            this.Text = "Table";
            ((System.ComponentModel.ISupportInitialize)(this.diceBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cpuBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dicePlayer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diceBox6)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox diceBox5;
        private System.Windows.Forms.PictureBox cpuBox3;
        private System.Windows.Forms.PictureBox cpuBox5;
        private System.Windows.Forms.PictureBox cpuBox1;
        private System.Windows.Forms.PictureBox playerBox;
        private System.Windows.Forms.PictureBox cpuBox2;
        private System.Windows.Forms.PictureBox cpuBox4;
        private System.Windows.Forms.PictureBox cpuBox6;
        private System.Windows.Forms.PictureBox diceBox3;
        private System.Windows.Forms.PictureBox diceBox1;
        private System.Windows.Forms.PictureBox dicePlayer;
        private System.Windows.Forms.PictureBox diceBox2;
        private System.Windows.Forms.PictureBox diceBox4;
        private System.Windows.Forms.PictureBox diceBox6;
        private System.Windows.Forms.Label cpuLabel5;
        private System.Windows.Forms.Label cpuLabel3;
        private System.Windows.Forms.Label cpuLabel1;
        private System.Windows.Forms.Label playerLabel;
        private System.Windows.Forms.Label cpuLabel2;
        private System.Windows.Forms.Label cpuLabel4;
        private System.Windows.Forms.Label cpuLabel6;
        private System.Windows.Forms.Label cpuTot5;
        private System.Windows.Forms.Label cpuTot3;
        private System.Windows.Forms.Label cpuTot1;
        private System.Windows.Forms.Label playerTot;
        private System.Windows.Forms.Label cpuTot2;
        private System.Windows.Forms.Label cpuTot4;
        private System.Windows.Forms.Label cpuTot6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button stopButton;
    }
}