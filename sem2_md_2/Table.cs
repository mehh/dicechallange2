﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Resources;

namespace sem2_md_2
{
    public partial class Table : Form
    {
        List<componentGroup> userList = new List<componentGroup>();
        List<componentGroup> tmp = new List<componentGroup>();
        int N = 7;
        int stop = 0;
        struct componentGroup {
            PictureBox picture;
            PictureBox dicePic;
            Label name;
            Label diceSum;
            Dice diceData;


            public componentGroup(PictureBox picture, PictureBox dicePic, Label name, Label diceSum, string username)
            {
                this.picture = picture;
                this.dicePic = dicePic;
                this.name = name;
                this.diceSum = diceSum;
                this.diceData = new Dice(username);
            }

            public PictureBox Picture { get => picture; set => picture = value; }
            public PictureBox DicePic { get => dicePic; set => dicePic = value; }
            public Label Name { get => name; set => name = value; }
            public Label DiceSum { get => diceSum; set => diceSum = value; }
            internal Dice DiceData { get => diceData; set => diceData = value; }

            public void visibility(int a)
            {
                switch (a)
                {
                    case 0:
                        this.picture.Visible = false;
                        this.dicePic.Visible = false;
                        this.name.Visible = false;
                        this.diceSum.Visible = false;
                        break;
                    case 1:
                        this.picture.Visible = true;
                        this.dicePic.Visible = true;
                        this.name.Visible = true;
                        this.diceSum.Text = "";
                        this.diceSum.Visible = true;
                        break;
                }
            }
        }

        public Table()
        {
            InitializeComponent();
            componentGroup cpu1 = new componentGroup(cpuBox1, diceBox1, cpuLabel1, cpuTot1, "CPU 1");
            componentGroup cpu2 = new componentGroup(cpuBox2, diceBox2, cpuLabel2, cpuTot2, "CPU 2");
            componentGroup cpu3 = new componentGroup(cpuBox3, diceBox3, cpuLabel3, cpuTot3, "CPU 3");
            componentGroup cpu4 = new componentGroup(cpuBox4, diceBox4, cpuLabel4, cpuTot4, "CPU 4");
            componentGroup cpu5 = new componentGroup(cpuBox5, diceBox5, cpuLabel5, cpuTot5, "CPU 5");
            componentGroup cpu6 = new componentGroup(cpuBox6, diceBox6, cpuLabel6, cpuTot6, "CPU 6");
            componentGroup player = new componentGroup(playerBox, dicePlayer, playerLabel, playerTot, "Player");
            userList.Add(player);
            userList.Add(cpu1);
            userList.Add(cpu2);
            userList.Add(cpu3);
            userList.Add(cpu4);
            userList.Add(cpu5);
            userList.Add(cpu6);
            clearTable();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Visible == true)
            {
                string text = textBox1.Text;
                int num;
                Int32.TryParse(text, out num);
                N = num + 1;
                if (num < 7 && num > 0)
                {
                    drawTable(num);
                    textBox1.Visible = false;
                    label1.Visible = false;
                    stopButton.Visible = true;
                    button1.Text = "Roll Dice!";
                }
            }
            else if(button1.Text == "Reset")
            {
                reset();
            }
            else
            {
                diceRoll();
            }
        }

        private void drawTable(int n)
        {
            clearTable();
            for (int i = 0; i < n + 1; i++)
            {
                userList.ElementAt(i).visibility(1);
            }
        }

        private void clearTable()
        {
            foreach(componentGroup c in userList)
            {
                c.visibility(0);
            }
        }

        private Bitmap getDiceImage(int val)
        {
            switch(val)
            {
                case 1:
                    return Properties.Resources.dice1;
                case 2:
                    return Properties.Resources.dice2;
                case 3:
                    return Properties.Resources.dice3;
                case 4:
                    return Properties.Resources.dice4;
                case 5:
                    return Properties.Resources.dice5;
                case 6:
                    return Properties.Resources.dice6;
                default:
                    return new Bitmap("fake");
            }
        }

        private void diceRoll()
        {
            int dice;
            for (int i = stop; i < N; i++)
            {
                if (!((userList.ElementAt(i).DiceData.SumOfDice >= 19) && userList.ElementAt(i).Name.Text.Contains("CPU")))
                {
                    userList.ElementAt(i).DiceData.throwDice();
                }
                userList.ElementAt(i).DiceSum.Text = "Total: " + userList.ElementAt(i).DiceData.SumOfDice.ToString();
                dice = userList.ElementAt(i).DiceData.LastThrow;
                userList.ElementAt(i).DicePic.BackgroundImage = getDiceImage(dice);
            }
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            stop++;
            for(int i = 0; i < 10; i++)
            {
                diceRoll();
            }
            calculateWinners();
            label1.Text = "Winners:\n";
            int z = 3;
            int count = 1;
            for (int i = 0; i < z; i++)
            {
                try
                {
                    if ((21 - tmp.ElementAt(i).DiceData.SumOfDice) < 0)
                    {
                        z++;
                        continue;
                    }
                }catch(System.IndexOutOfRangeException)
                {
                    return;
                }
                if (tmp.ElementAt(i).Name.Visible == true)
                {
                    label1.Text += count + ": " + tmp.ElementAt(i).Name.Text + "\n";
                }
                count++;
            }
            label1.Visible = true;
            button1.Text = "Reset";
        }

        private void calculateWinners()
        {
            //stackoverflow.com/questions/20902248/sorting-a-list-in-c-sharp-using-list-sortcomparisont-comparison
            tmp = userList.OrderBy(i => 21 - i.DiceData.SumOfDice).ToList();
        }

        private void reset()
        {
            for(int i = 0; i < userList.Count; i++)
            {
                userList.ElementAt(i).DiceData.SumOfDice = 0;
                userList.ElementAt(i).DiceData.LastThrow = 0;
                userList.ElementAt(i).DiceData.History = new List<int>();
                if (userList.ElementAt(i).DicePic.Image != null) // stackoverflow.com/questions/5856196/clear-image-on-picturebox
                {
                    userList.ElementAt(i).DicePic.Image.Dispose();
                    userList.ElementAt(i).DicePic.Image = null;
                }
            }
            button1.Text = "Enter";
            label1.Text = "Enter number of CPU players(1-6)";
            textBox1.Visible = true;
            label1.Visible = true;
            stopButton.Visible = false;
            stop--;
            clearTable();
        }
    }
}
