﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace sem2_md_2
{
    class Dice
    {
        public static Random MyRNG = new Random();
        string playerPseudoname;
        List<int> history;
        int lastThrow;
        int sumOfDice;

        public Dice(string playerPseudoname)
        {
            this.playerPseudoname = playerPseudoname;
            this.history = new List<int>();
            this.lastThrow = 0;
            this.sumOfDice = 0;
        }

        public string PlayerPseudoname { get => playerPseudoname; set => playerPseudoname = value; }
        public List<int> History { get => history; set => history = value; }
        public int LastThrow { get => lastThrow; set => lastThrow = value; }
        public int SumOfDice { get => sumOfDice; set => sumOfDice = value; }

        public void throwDice()
        {
            int tmp;
            lock (MyRNG)
            {
                tmp = MyRNG.Next(1, 7);
            }
            this.history.Add(tmp);
            this.lastThrow = tmp;
            this.sumOfDice += tmp;
        }

        public string showInfo()
        {
            return this.PlayerPseudoname + " total: " + SumOfDice;
        }
    }
}
